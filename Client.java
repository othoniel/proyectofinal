package example.hello;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Client implements GoodBye {

    private Client() {}

    public String sayGoodBye() {
        return "GoodBye, Motherfuckers!";
    }

    public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
            Client obj = new Client();
            GoodBye stubcito = (GoodBye) UnicastRemoteObject.exportObject(obj, 0);
            Registry registryClient = LocateRegistry.createRegistry(9002);
            registryClient.bind("GoodBye", stubcito);

            Registry registryServer = LocateRegistry.getRegistry(9001);
            Hello stub = (Hello) registryServer.lookup("Hello");
            String response = stub.sayHello();
            System.out.println("response: " + response);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}