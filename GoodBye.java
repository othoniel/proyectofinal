package example.hello;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GoodBye extends Remote {
    String sayGoodBye() throws RemoteException;
}