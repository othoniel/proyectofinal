package example.hello;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Server implements Hello {

    public Server() {}

    public String sayHello() {
        return "Hello, world!";
    }

    public static void main(String args[]) {

        try {
            Server obj = new Server();
            Hello stub = (Hello) UnicastRemoteObject.exportObject(obj, 0);

            // Bind the remote object's stub in the registry
            Registry registryServer = LocateRegistry.createRegistry(9001);
            registryServer.bind("Hello", stub);

            System.err.println("Server ready");

            Thread.sleep(3000);

            Registry registryClient = LocateRegistry.getRegistry(9002);
            GoodBye stubcito = (GoodBye) registryClient.lookup("GoodBye");
            String response = stubcito.sayGoodBye();
            System.out.println("response: " + response);
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}